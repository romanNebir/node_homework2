require('dotenv').config();
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const path = require('path');
const exphbs = require('express-handlebars');
const morgan = require('morgan');
const mainRouter = require('./routers/mainRouter');
const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const notesRouter = require('./routers/notesRouter');

const app = express();
const PORT = process.env.PORT;

const hbs = exphbs.create({
  defaultLayout: 'main',
  extname: 'hbs',
});
app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.set('views', 'views');

app.use(cors());
app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(morgan('tiny'));

app.get('/', mainRouter);
app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/notes', notesRouter);

/**
 * new error class.
 * @param {str} message message.
 */
class UnauthorizedError extends Error {
  /**
 * new error class.
 * @param {str} message message.
 */
  constructor(message = 'Unauthorized user!') {
    super(message);
    statusCode = 401;
  }
}

app.use((err, req, res, next) => {
  if (err instanceof UnauthorizedError) {
    res.status(err.statusCode).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

/**
 * starts the server
 */
function start() {
  mongoose.connect(process.env.DB,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      })
      .then(() =>
        app.listen(PORT, () => {
          console.log(`Server has been started on port ${PORT}...`);
        }),
      )
      .catch((error) => console.log('Mongo error: ' + error.message));
}

start();
