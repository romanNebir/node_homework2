const express = require('express');
// const { isAuthorized } = require("./middlewares/authMiddleware");
const {getMainPage} = require('../controllers/mainController');
const {asyncWrapper} = require('./helpers');

const router = new express.Router();

// router.get("/", asyncWrapper(isAuthorized), asyncWrapper(getMainPage));
router.get('/', asyncWrapper(getMainPage));

module.exports = router;
