const express = require('express');
const {
  getNotes,
  addNote,
  getNote,
  updateNote,
  checkNote,
  deleteNote,
} = require('../controllers/notesController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {asyncWrapper} = require('./helpers');

const router = new express.Router();

router.use('*', authMiddleware);
router.get('/', asyncWrapper(getNotes));
router.post('/', asyncWrapper(addNote));
router.get('/:id', asyncWrapper(getNote));
router.put('/:id', asyncWrapper(updateNote));
router.patch('/:id', asyncWrapper(checkNote));
router.delete('/:id', asyncWrapper(deleteNote));

module.exports = router;
