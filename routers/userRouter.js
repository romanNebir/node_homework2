const express = require('express');
const {
  getUser,
  deleteUser,
  changePassword,
} = require('../controllers/userController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {asyncWrapper} = require('./helpers');

const router = new express.Router();

router.use('*', authMiddleware);
router.get('/', asyncWrapper(getUser));
router.delete('/', asyncWrapper(deleteUser));
router.patch('/', asyncWrapper(changePassword));

module.exports = router;
