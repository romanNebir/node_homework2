const express = require('express');
const {registration, login} = require('../controllers/authController');
const {validateRegistration} = require('./middlewares/validationMiddleware');
const {asyncWrapper} = require('./helpers');

const router = new express.Router();

router.post(
    '/register',
    asyncWrapper(validateRegistration),
    asyncWrapper(registration),
);
router.post('/login', asyncWrapper(login));

module.exports = router;
