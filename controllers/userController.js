const bcrypt = require('bcrypt');
const {User}= require('../models/userModel');

module.exports.getUser = async (req, res) => {
  const user = await User.findById(req.user._id);
  res.json({user});
};

module.exports.deleteUser = async (req, res) => {
  await User.findByIdAndDelete(req.user._id);
  res.json({message: 'User successfully deleted'});
};

module.exports.changePassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById(req.user._id);
  if ( !(await bcrypt.compare(oldPassword, user.password)) ) {
    return res.status(400).json({message: `Wrong password!`});
  }
  const password = await bcrypt.hash(newPassword, 10);
  User.findByIdAndUpdate(user.id, {password}, (err, doc) => {
    if (err) {
      return res.status(500).json({message: 'ups, something went wrong'});
    }
    res.status(200).json({message: 'password succsesfully changed'});
  });
};
