const {Note} = require('../models/noteModel');

module.exports.getNotes = async (req, res) => {
  const userId = req.user._id;
  const limit = parseInt(req.query.limit);
  const offset = parseInt(req.query.offset);
  try {
    const notes = await Note.find({userId}).skip(offset).limit(limit);
    res.json({notes});
  } catch (e) {
    res.status(400).json({message: 'error'});
  }
};

module.exports.addNote = async (req, res) => {
  const note = new Note({
    userId: req.user._id,
    text: req.body.text,
  });
  try {
    await note.save();
    res.json({message: 'Note created successfully'});
  } catch (e) {
    res.status(400).json({message: 'error'});
  }
};

module.exports.getNote = async (req, res) => {
  try {
    const note = await Note.findById(req.params.id);
    if (!note) {
      return res.status(400).json({message: 'There is no note with such id'});
    }
    res.json({note});
  } catch (e) {
    res.status(400).json({message: 'ups, something went wrong'});
  }
};

module.exports.updateNote = async (req, res) => {
  try {
    await Note.findByIdAndUpdate(req.params.id, {text: req.body.text});
    res.json({message: 'note updated successfully'});
  } catch (e) {
    res.status(400).json({message: 'error'});
  }
};

module.exports.checkNote = async (req, res) => {
  try {
    const note = await Note.findById(req.params.id);
    note.completed = !note.completed;
    await note.save();
    res.json({message: 'success'});
  } catch (e) {
    res.status(400).json({message: 'error'});
  }
};

module.exports.deleteNote = async (req, res) => {
  try {
    await Note.findByIdAndDelete(req.params.id);
    res.json({message: 'success'});
  } catch (e) {
    res.status(400).json({message: 'error'});
  }
};

